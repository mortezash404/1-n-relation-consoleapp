﻿using System;

namespace RelationConsoleApp.Entities
{
    public class Order
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }
        public DateTime OrderDateTime { get; set; }
        public int? CustomerId { get; set; }
    }
}
