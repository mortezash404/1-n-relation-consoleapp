﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using RelationConsoleApp.DAL;
using RelationConsoleApp.Entities;

namespace RelationConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new AppDbContext();

            // با این دستور نیازی به مایگریشن نیست اما با هر تغییر در مدل ها جداول باید از ابتدا ساخته شوند

            context.Database.EnsureCreated();

            // اضافه کردن مشتری بدون ثبت سفارش در جدول مشتری اطلاعات مشتری ثبت می شود ولی در جدول سفارش چیزی ثبت نمی شود
            
            context.Customers.Add(new Customer
            {
                Name = "Ali",
                Phone = "0918"
            });

            // اضافه کردن مشتری با ثبت سفارش در جدول مشتری اطلاعات مشتری ثبت می شود و در جدول سفارش آیدی مشتری به عنوان کلید خارجی ثبت می شود

            context.Customers.Add(new Customer
            {
                Name = "Reza",
                Phone = "0912",
                Orders = new List<Order>
                {
                    new Order
                    {
                        Title = "وسایل بهداشتی",
                        Price = 2500,
                        OrderDateTime = DateTime.Now
                    }
                }
            });

            context.SaveChanges();

            // لیست تمام مشتریان بدون سفارشات

            var customers = context.Customers.ToList();

            // لیست تمام مشتریان همراه با سفارشات آنها

            var customersOrders = context.Customers.Include(c=>c.Orders).ToList();
        }
    }
}
