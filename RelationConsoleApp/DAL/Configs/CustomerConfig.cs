﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RelationConsoleApp.Entities;

namespace RelationConsoleApp.DAL.Configs
{
    public class CustomerConfig : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.ToTable("Customer");
            builder.Property(p => p.Name).HasMaxLength(20);
            builder.Property(p => p.Phone).HasMaxLength(16);

            // تعریف رابطه یک به چند که اغلب از سمت یک رابطه صورت می گیرد
            // in many side we dont have navigation property
            // public Customer Customer { get; set; }
            // so use WithOne() otherwise we use : WithOne(w => w.Customer)

            builder.HasMany(p=>p.Orders).WithOne().HasForeignKey(c=>c.CustomerId);
        }
    }
}
