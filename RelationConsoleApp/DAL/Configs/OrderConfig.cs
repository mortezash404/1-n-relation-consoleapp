﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RelationConsoleApp.Entities;

namespace RelationConsoleApp.DAL.Configs
{
    public class OrderConfig : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.ToTable("Order");
            builder.Property(p => p.Title).HasMaxLength(20);
            builder.Property(p => p.OrderDateTime).HasColumnType("datetime2");
        }
    }
}
